//
//  VTAccelCollectorViewController.m
//  VTNodeConnectionHelper
//
//  Created by Wade Gasior on 3/30/15.
//  Copyright (c) 2015 Variable, Inc. All rights reserved.
//

#import "VTAccelCollectorViewController.h"
#import <Node_iOS/Node_iOS.h>
#import "MBProgressHUD.h"

@interface VTAccelCollectorViewController ()<NodeDeviceDelegate>
@property (weak, nonatomic) IBOutlet UILabel *topLabel;
@property (weak, nonatomic) IBOutlet UIButton *startStopButton;
@property (strong, nonatomic) MBProgressHUD *hud;
@property (weak, nonatomic) IBOutlet UILabel *collectingAccelLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic        ) uint32_t                         numReadings;
@property (nonatomic        ) uint32_t                         numReadingsReceived;
@property (nonatomic        ) double                           hsaPeriod;
@property (nonatomic        ) double                           hsaCurrentTimestamp;

@property (strong, nonatomic) NSDate                           *sDate;
@property (strong, nonatomic) NSDate                           *eDate;

@property (nonatomic) BOOL isCollectingAccelData;

@end

@implementation VTAccelCollectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.nodeDevice.delegate = self;
    self.topLabel.text = [NSString stringWithFormat: @"Connected to %@", self.nodeDevice.peripheral.name];
    
    self.activityIndicator.hidden = true;
    self.collectingAccelLabel.hidden = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)handleDone:(id)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)handleStartStop:(id)sender {
    if(self.isCollectingAccelData == false) {
        self.isCollectingAccelData = true;
        [self.startStopButton setTitle:@"Stop Collecting" forState:UIControlStateNormal];
        [self startHSACollectionProcess];
    }
    else {
        self.isCollectingAccelData = false;
        [self.startStopButton setTitle:@"Start Collecting" forState:UIControlStateNormal];
        [self stopHSACollectionProcess];
    }
}

////////////////////////////////////////////////////////////////////////////////
// HSA = High Speed Accel
////////////////////////////////////////////////////////////////////////////////
- (void) startHSACollectionProcess
{
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = @"Erasing Flash";
    
    //Step 1: Clear the flash memory
    [self.nodeDevice requestDataLoggingMemoryEraseBlocksFrom:0 to:10];
}

//Callback for step 1 (flash memory has been erased)
- (void)nodeDeviceDidCompleteDataloggingOperation:(VTNodeDevice *)device operation:(VTNodeDataLoggingOperation)op
{
    if(op == VTnodeDataLoggingOperation_MemEraseComplete) {
        NSLog(@"Data log erase complete");
        NSInteger sampleRate = [[NSUserDefaults standardUserDefaults] integerForKey:@"sampleRate"];
        
        //Step 2: Start accel collection
        [self.nodeDevice startHighSpeedAccelerometerWithTargetHz:sampleRate usingAutoStart:NO usingAutoStop:NO];
    }
}

//Callback DURING step 2 - handle the "real time" readings - These come in at 30 Hz
- (void)nodeDeviceDidUpdateAccReading:(VTNodeDevice *)device withReading:(VTSensorReading *)reading
{
    NSLog(@"Got real time reading: %0.2f %0.2f %0.2f", reading.x, reading.y, reading.z);
    if(self.isCollectingAccelData) {
        [self.hud hide:YES];
        self.activityIndicator.hidden = false;
        self.collectingAccelLabel.hidden = false;
    }
}

- (void) stopHSACollectionProcess
{
    self.activityIndicator.hidden = true;
    self.collectingAccelLabel.hidden = true;
    [self.nodeDevice stopHighSpeedAccelerometer];
    [self.hud hide:YES];
    
    [self showDownloadDataPrompt];
}

// After we stop collection, make sure we want to download the results (this is going to take a while)
- (void) showDownloadDataPrompt
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Collection Complete"
                                                                             message:@"Would you like to download the readings?"
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Discard" style:UIAlertActionStyleDestructive handler:^(UIAlertAction *action) {
        [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Download" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        [self startHSADataFetch];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

// Step 4: Request the readings
- (void) startHSADataFetch
{
    //Show data fetch hud
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.mode = MBProgressHUDModeAnnularDeterminate;
    self.hud.labelText = @"Fetching Readings";

    //API call to start the download process
    [self.nodeDevice startHighSpeedAccelerometerDataFetch];
}

//Callback - HSA transmission start message
- (void) nodeDeviceDidBeginHSATransmission:(VTNodeDevice *)device withNumberOfReadings:(uint32_t)numReadings withFrequency:(float)freq forWave:(uint8_t)wave withNumberOfWaves:(uint8_t)numWaves
{
    NSLog(@"HSA TX START");
    self.numReadings         = numReadings;
    self.numReadingsReceived = 0;
    self.hsaPeriod           = 1.0 / freq;
    self.hsaCurrentTimestamp = 0;
    self.sDate               = [NSDate date];
}

//API Callback for each high speed accel. reading that is transmitted
- (void)nodeDeviceDidTransmitHSAReading:(VTNodeDevice *)device reading:(VTSensorReading *)reading
{
    self.numReadingsReceived++;
    self.hud.progress = (float)self.numReadingsReceived / self.numReadings;
    
    NSDictionary *motionReading = @{
                              @"x": @(reading.x),
                              @"y": @(reading.y),
                              @"z": @(reading.z),
                              @"time": @(self.hsaCurrentTimestamp)
                              };
    
    NSLog(@"%@", motionReading);

    _hsaCurrentTimestamp += _hsaPeriod;
    
    ////////////////////////////////////////////////////////////////////////////////
    /// Calculate time left
    ////////////////////////////////////////////////////////////////////////////////
    NSLog(@"RX'ed %d of %d", self.numReadingsReceived, self.numReadings);
    if(self.numReadingsReceived > 50) {
        NSTimeInterval secondsLeft = (_numReadings - _numReadingsReceived) /  (_numReadingsReceived / [[NSDate date] timeIntervalSinceDate:_sDate]);
        self.hud.labelText = [NSString stringWithFormat: @"%02i:%02i", (int)secondsLeft/60, (int)fmod(secondsLeft, 60)];
    }
}

//API Call - happens when NODE is finished transmitting HSA readings
- (void)nodeDeviceDidCompleteHSATransmission:(VTNodeDevice *)device
{
    NSLog(@"************ HSA TX COMPLETE ***************");
    self.eDate = [NSDate date];
    NSLog(@"Got %d readings in %f s", self.numReadings, [self.eDate timeIntervalSinceDate:self.sDate]);
    [self.hud hide:YES];
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Collection Complete"
                                                                             message:@"Accel values have been collected and transmitted."
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
