//
//  VTAccelCollectorViewController.h
//  VTNodeConnectionHelper
//
//  Created by Wade Gasior on 3/30/15.
//  Copyright (c) 2015 Variable, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
@class VTNodeDevice;

@interface VTAccelCollectorViewController : UIViewController

@property (strong, nonatomic) VTNodeDevice *nodeDevice;

@end
