#ifndef Node_iOS_VTColorUtils_h
#define Node_iOS_VTColorUtils_h

typedef NS_ENUM(NSInteger, VTColorUtilsWhitePointRef) {
    VTColorUtilsWhitePointRefD50,
    VTColorUtilsWhitePointRefD65
};

typedef NS_ENUM(NSInteger, VTColorUtilsRGBSpace) {
    VTColorUtilsRGBSpace_sRGB
};

#endif
