//
//  VTRGBColor.h
//  Node_iOS
//
//  Created by Wade Gasior on 1/7/15.
//  Copyright (c) 2015 Variable, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "VTColorUtils.h"

@interface VTRGBColor : NSObject

#if TARGET_OS_IPHONE
#define SKColor UIColor
#else
#define SKColor NSColor
#endif

@property (nonatomic, readonly) double r;
@property (nonatomic, readonly) double g;
@property (nonatomic, readonly) double b;
@property (nonatomic, readonly) VTColorUtilsRGBSpace rgbSpace;

+ (instancetype) rgbColorWithR: (double) r withG: (double)g withB: (double) b inSpace: (VTColorUtilsRGBSpace) rgbSpace;
- (SKColor *) color;

@end
