#import <Foundation/Foundation.h>
#import "VTXYZColor.h"
#import "VTColorUtils.h"

@interface VTLabColor : NSObject
@property (nonatomic, readonly) double L;
@property (nonatomic, readonly) double A;
@property (nonatomic, readonly) double B;
@property (nonatomic, readonly) VTColorUtilsWhitePointRef whitePointRef;

+ (instancetype) labColorWithL: (double) l a: (double) a b: (double) b usingRef: (VTColorUtilsWhitePointRef) whitePointRef;
- (VTXYZColor *) XYZColor;

@end
