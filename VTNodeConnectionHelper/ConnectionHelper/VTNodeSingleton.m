//
//  VTNodeSingleton.m
//  VTNodeConnectionHelper
//
//  Created by Wade Gasior on 1/8/15.
//  Copyright (c) 2015 Variable, Inc. All rights reserved.
//

#import "VTNodeSingleton.h"

@implementation VTNodeSingleton

+ (instancetype) getInstance
{
    static VTNodeSingleton *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[VTNodeSingleton alloc] init];
        instance.nodeConnectionHelper = [[VTNodeConnectionHelper alloc] initWithDelegate:instance];
    });
    
    return instance;
}

- (void)nodeConnectionHelperDidUpdateNodeDeviceList
{
    //Do nothing - this is handled by other VCs that grab the delegate
}

- (void) nodeConnectionHelperDidDisconnectFromPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    self.nodeDevice = nil;
}

@end
