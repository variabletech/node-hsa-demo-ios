//
//  VTNodeSingleton.h
//  VTNodeConnectionHelper
//
//  Created by Wade Gasior on 1/8/15.
//  Copyright (c) 2015 Variable, Inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Node_iOS/Node_iOS.h>
#import "VTNodeConnectionHelper.h"

@interface VTNodeSingleton : NSObject <VTNodeConnectionHelperDelegate>

+ (instancetype) getInstance;

@property (strong, nonatomic) VTNodeDevice *nodeDevice;
@property (strong, nonatomic) VTNodeConnectionHelper *nodeConnectionHelper;

@end
